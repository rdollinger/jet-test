# -*- coding: utf-8 -*-
import datetime

from django.db import models
from django.utils.translation import ugettext_lazy as _


class Auftragspositionen(models.Model):
    titel = models.CharField(max_length=250, blank=False, default='', verbose_name=_('Titel'))
    beschreibung = models.TextField(blank=False, verbose_name=_('Beschreibung'))

    class Meta:
        verbose_name = "Auftragsposition"
        verbose_name_plural = "Auftragspositionen"
        ordering = ['titel']

    def __str__(self):
        return self.titel


class Leistungen(models.Model):
    auftragsposition = models.ForeignKey(Auftragspositionen, blank=False,
                                         related_name='%(app_label)s_%(class)s_auftragsposition',
                                         verbose_name=_('Auftragsposition'))
    datum = models.DateField(default=datetime.datetime.today, verbose_name=_('Datum'))
    stunden = models.DecimalField(blank=False, max_digits=10, decimal_places=2, verbose_name=_('Stunden'))
    beschreibung = models.TextField(blank=True, verbose_name=_('Beschreibung'))

    class Meta:
        verbose_name = "Leistung"
        verbose_name_plural = "Leistungen"
        ordering = ['-datum']

    def __str__(self):
        return "%s - %s" % (self.datum.strftime('%d.%m.%Y'), self.beschreibung)
