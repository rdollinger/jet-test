# -*- coding: utf-8 -*-
from django.contrib import admin

from jet.admin import CompactInline

from .models import Auftragspositionen, Leistungen


class LeistungenInline(CompactInline):
    model = Leistungen
    extra = 0
    exclude = ()


class AuftragspositionenAdmin(admin.ModelAdmin):
    list_display = ('titel', 'beschreibung', )
    list_filter = ()
    actions = []

    inlines = [
        LeistungenInline,
    ]


admin.site.register(Auftragspositionen, AuftragspositionenAdmin)


class LeistungenAdmin(admin.ModelAdmin):
    list_display = ('datum', 'stunden', 'beschreibung', )
    list_filter = ('auftragsposition', 'datum', )
    exclude = ()


admin.site.register(Leistungen, LeistungenAdmin)
